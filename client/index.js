const sessionId = "qFCHqIKptEMq2TFD45+vvg==";
const url = "http://localhost:3000";
const path = "/user";
const method = "POST";
const headers = {
  "Content-Type": "application/json",
  Cookie: `sessionID=${sessionId}`,
};

const errors = [];
let responseError;

function isValidName(name) {
  const minLength = 1;
  const maxLength = 255;

  return name.trim().length >= minLength && name.trim().length <= maxLength;
}

function isValidEmail(email) {
  if (email.trim().length === 0) {
    return false;
  }

  const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
  return emailRegex.test(email);
}

function isValidPassword(password) {
  const minLength = 8;
  if (password.trim().length < minLength) {
    return false;
  }

  return true;
}

function isValidForm(firstName, lastName, email, password) {
  console.log(errors);
  if (!firstName) {
    errors.push("First Name cannot be empty");
    return false;
  }

  if (!isValidName(firstName)) {
    errors.push("First Name cannot be more than 255 characters");
    return false;
  }

  if (!lastName) {
    errors.push("Last Name cannot be empty");
    return false;
  }

  if (!isValidName(lastName)) {
    errors.push("Last Name cannot be more than 255 characters");
    return false;
  }

  if (!email) {
    errors.push("Email cannot be empty");
    return false;
  }

  if (!isValidEmail(email)) {
    errors.push("This is not a valid email");
    return false;
  }

  if (!password) {
    errors.push("Password cannot be empty");
    return false;
  }

  if (!isValidPassword(password)) {
    errors.push("Password should be at least 8 characters");
    return false;
  }

  return true;
}

function makeRequestToServer(url, path, method, headers, body, redirectPath) {
  fetch(url + path, {
    method,
    headers,
    body: JSON.stringify(body),
    credentials: "include",
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .then((data) => {
      console.log(data);
      if (path === "/logout" || (path === "/user" && method === "DELETE")) {
        localStorage.removeItem("userData");
      } else {
        localStorage.setItem("userData", JSON.stringify(data));
        const userData = JSON.parse(localStorage.getItem("userData"));
        console.log(userData.email);
      }

      if (redirectPath) {
        window.location.href = redirectPath;
      }
    })
    .catch((error) => {
      responseError = error;
      console.error(error);
    });
}

function login(email, password, redirectPath) {
  const headers = {
    "Content-Type": "application/json",
  };
  const body = {
    email,
    password,
  };

  makeRequestToServer(url, "/login", "POST", headers, body, redirectPath);
}

function logout() {
  const headers = {
    "Content-Type": "application/json",
  };
  const body = {};
  makeRequestToServer(url, "/logout", "POST", headers, body);
}

function register(firstName, lastName, email, password) {
  const headers = {
    "Content-Type": "application/json",
  };
  const body = {
    firstName,
    lastName,
    email,
    password,
  };

  console.log(body);
  makeRequestToServer(url, "/user", "POST", headers, body);
}

function updateUser(
  sessionId,
  updatedFirstName,
  updatedLastName,
  updatedEmail,
  updatedPassword,
  redirectPath
) {
  const headers = {
    "Content-Type": "application/json",
  };
  const body = {
    sessionId,
    updatedFirstName,
    updatedLastName,
    updatedEmail,
    updatedPassword,
  };
  makeRequestToServer(url, "/user", "PUT", headers, body, redirectPath);
}

function deleteUser(sessionId, redirectPath) {
  const headers = {
    "Content-Type": "application/json",
  };
  const body = { sessionId };
  makeRequestToServer(url, "/user", "DELETE", headers, body, redirectPath);
}

function onRegister() {
  const redirectPath = "./login.html";
  console.log("register");
  const firstName = document.getElementById("firstName").value;
  const lastName = document.getElementById("lastName").value;
  const email = document.getElementById("email").value;
  const password = document.getElementById("password").value;

  if (!isValidForm(firstName, lastName, email, password)) {
    populateErrorFields(errors[0]);
    return;
  }

  register(firstName, lastName, email, password, redirectPath);
}

function onLogin() {
  const redirectPath = "./profile.html";
  console.log("login");
  const email = document.getElementById("email").value;
  const password = document.getElementById("password").value;

  if (!email || !password) {
    console.log("Missing Field");
    return;
  }

  login(email, password, redirectPath);
}

function onUpdateUser() {
  const redirectPath = "./profile.html";
  console.log("update");
  const firstName = document.getElementById("firstName").value;
  const lastName = document.getElementById("lastName").value;
  const email = document.getElementById("email").value;
  const password = document.getElementById("password").value;

  if (!isValidForm(firstName, lastName, email, password)) {
    return;
  }

  const sessionId = JSON.parse(localStorage.getItem("userData")).sessionId;

  updateUser(sessionId, firstName, lastName, email, password, redirectPath);
}

function onDeleteUser() {
  const redirectPath = "./login.html";
  const sessionId = JSON.parse(localStorage.getItem("userData")).sessionId;

  deleteUser(sessionId, redirectPath);
}

function populateProfileFields() {
  const userData = JSON.parse(localStorage.getItem("userData"));

  document.getElementById("pfFirstName").innerHTML = `${userData.firstName}`;
  document.getElementById("pfLastName").innerHTML = `${userData.lastName}`;
  document.getElementById("pfEmail").innerHTML = `${userData.email}`;
}
populateProfileFields();

function populateErrorFields(error) {
  document.getElementById("registerError").innerHTML = `${error}`;
}

document.addEventListener("DOMContentLoaded", () => {
  document
    .getElementById("registerForm")
    .addEventListener("submit", function (event) {
      event.preventDefault();
      onRegister();
    });

  document
    .getElementById("loginForm")
    .addEventListener("submit", function (event) {
      event.preventDefault();
      onLogin();
    });

  document
    .getElementById("updateForm")
    .addEventListener("submit", function (event) {
      event.preventDefault();
      onUpdateUser();
    });

  document
    .getElementById("deleteForm")
    .addEventListener("submit", function (event) {
      event.preventDefault();
      onDeleteUser();
    });
});
