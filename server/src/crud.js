const path = require("path");
const dotenv = require("dotenv");
const { Pool } = require("pg");
const { hashPassword } = require("./auth.js");

dotenv.config({
  override: true,
  path: path.join(__dirname, "../../.env"),
});

const pool = new Pool({
  user: process.env.USER,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  host: process.env.HOST,
  port: process.env.PORT,
});

async function createUsersTable() {
  try {
    const result = await pool.query(
      "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = $1)",
      ["users"]
    );

    if (!result.rows[0].exists) {
      await pool.query(
        "CREATE TABLE users (id SERIAL PRIMARY KEY, firstName VARCHAR(50) NOT NULL, lastName VARCHAR(50) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL)"
      );
    }
  } catch (error) {
    console.error(error.message);
  }
}

async function hasUserWithEmail(email) {
  try {
    const result = await pool.query(
      `
              SELECT * FROM users WHERE email = $1
            `,
      [email]
    );

    return result.rows ? result.rows.length > 0 : false;
  } catch (error) {
    throw error;
  }
}

async function createUser(userData) {
  try {
    const hashedPassword = await hashPassword(userData.password);
    const result = await pool.query(
      `
              INSERT INTO users (firstName, lastName, email, password)
              VALUES ($1, $2, $3, $4)
            `,
      [userData.firstName, userData.lastName, userData.email, hashedPassword]
    );

    return result;
  } catch (error) {
    throw error;
  }
}

async function updateUser(userId, updatedFields) {
  try {
    const { updatedFirstName, updatedLastName, updatedEmail, updatedPassword } =
      updatedFields;

    const hashedPassword = await hashPassword(updatedPassword);

    const result = await pool.query(
      `
        UPDATE users
        SET firstName = $1, lastName = $2, email = $3, password = $4
        WHERE id = $5
        `,
      [updatedFirstName, updatedLastName, updatedEmail, hashedPassword, userId]
    );

    return result;
  } catch (error) {
    throw error;
  }
}

async function deleteUser(userId) {
  try {
    const result = await pool.query(
      `
            DELETE FROM users
            WHERE id = $1
            `,
      [userId]
    );

    return result;
  } catch (error) {
    throw error;
  }
}

async function dbTest() {
  const client = await pool.connect();

  try {
    const { rows } = await pool.query("SELECT * FROM users");

    console.log(rows);
  } catch (error) {
    console.log(error);
  } finally {
    client.release();
  }
}

module.exports = {
  createUsersTable,
  createUser,
  updateUser,
  deleteUser,
  dbTest,
  hasUserWithEmail,
};
