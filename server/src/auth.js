const crypto = require("crypto");
const bcrypt = require("bcrypt");
const path = require("path");
const dotenv = require("dotenv");
const { Pool } = require("pg");

dotenv.config({
  override: true,
  path: path.join(__dirname, "../../.env"),
});

const pool = new Pool({
  user: process.env.USER,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  host: process.env.HOST,
  port: process.env.PORT,
});

const sessions = {};

function generateSessionId() {
  return crypto.randomBytes(16).toString("base64");
}

function parseCookies(cookieHeader) {
  return cookieHeader
    ? cookieHeader.split(";").reduce((acc, cookie) => {
        const [name, value] = cookie.trim().split("=");
        acc[name] = value;
        return acc;
      }, {})
    : {};
}

async function hashPassword(password) {
  const saltRounds = 10;
  return bcrypt.hash(password, saltRounds);
}

async function comparePasswords(password, hashedPassword) {
  return bcrypt.compare(password, hashedPassword);
}

async function loginUser(email, password) {
  const result = await pool.query(
    `
        SELECT * FROM users WHERE email = $1
      `,
    [email]
  );

  if (result.rows.length === 0) {
    return null;
  }

  const user = result.rows[0];
  const passwordMatch = await comparePasswords(password, user.password);

  return passwordMatch ? user : null;
}

async function logoutUser(sessionId) {
  console.log(sessionId);
  if (sessionId && sessions[sessionId]) {
    console.log("delete");
    delete sessions[sessionId];
  }
}

module.exports = {
  sessions,
  generateSessionId,
  parseCookies,
  loginUser,
  logoutUser,
  hashPassword,
};
