const http = require("http");
const urlLib = require("url");
const cors = require("cors");
const {
  createUsersTable,
  dbTest,
  hasUserWithEmail,
  createUser,
  updateUser,
  deleteUser,
} = require("./src/crud.js");
const {
  loginUser,
  logoutUser,
  parseCookies,
  generateSessionId,
  sessions,
} = require("./src/auth.js");

function getRequestBody(req) {
  return new Promise((resolve, reject) => {
    let data = "";
    req.on("data", (chunk) => (data += chunk));
    req.on("end", () => resolve(JSON.parse(data)));
    req.on("error", (error) => reject(error));
  });
}

async function handleRequestRouting(req, res) {
  try {
    const { method, url, headers } = req;
    const { path } = urlLib.parse(url);

    if (path === "/user" && method === "POST") {
      const data = await getRequestBody(req);
      console.log("add user", data);

      const userWithEmailExits = await hasUserWithEmail(data.email);

      if (userWithEmailExits) {
        res.writeHead(409, { "Content-Type": "application/json" });
        res.end();
      } else {
        const user = await createUser(data);

        res.writeHead(200, { "Content-Type": "application/json" });
        res.end();
      }
      return;
    } else if (path === "/user" && method === "PUT") {
      const data = await getRequestBody(req);
      console.log("update user", data);
      const userSession = sessions[data.sessionId];

      const user = await updateUser(userSession.id, data);

      res.writeHead(200, { "Content-Type": "application/json" });
      const responseData = JSON.stringify({
        firstName: user.firstname,
        lastName: user.lastname,
        email: user.email,
        sessionId: data.sessionId,
      });
      res.end(responseData);
      return;
    } else if (path === "/user" && method === "DELETE") {
      const data = await getRequestBody(req);
      console.log("delete user", data);

      const userSession = sessions[data.sessionId];
      if (!userSession) {
        res.writeHead(404, { "Content-Type": "application/json" });
        res.end();
        return;
      }

      const deletedUser = await deleteUser(userSession.id);
      await logoutUser(data.sessionId);

      res.writeHead(200, { "Content-Type": "application/json" });
      res.end();
      return;
    } else if (path === "/login" && method === "POST") {
      const data = await getRequestBody(req);
      console.log("login", data);

      const user = await loginUser(data.email, data.password);
      if (user) {
        const sessionId = generateSessionId();
        sessions[sessionId] = user;

        res.writeHead(200, {
          "Content-Type": "application/json",
          "Set-Cookie": "sessionId=" + sessionId,
        });
        const responseData = JSON.stringify({
          firstName: user.firstname,
          lastName: user.lastname,
          email: user.email,
          sessionId: sessionId,
        });

        res.end(responseData);
      } else {
        res.writeHead(401, { "Content-Type": "application/json" });
        res.end();
      }

      return;
    } else if (path === "/logout" && method === "POST") {
      /*
      const cookies = parseCookies(headers.cookie);
      await logoutUser(cookies.sessionId);

      res.setHeader(
        "Set-Cookie",
        "sessionId=; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT"
      );
      */
      const data = await getRequestBody(req);
      await logoutUser(data.sessionId);

      res.writeHead(200, { "Content-Type": "application/json" });
      res.end();
    } else {
      res.end();
      return;
    }
  } catch (error) {
    console.log("Error: ", error.message);

    res.writeHead(500, { "Content-Type": "text/plain" });
    res.end("Internal Server Error");
    return;
  }
}

const PORT = process.env.NODE_PORT;

const corsMiddleware = cors({
  origin: "null",
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  credentials: true,
  optionsSuccessStatus: 204,
});

const server = http.createServer((req, res) => {
  corsMiddleware(req, res, () => {
    handleRequestRouting(req, res);
  });
});

createUsersTable();
dbTest();

server.listen(PORT, () => {
  console.log(`Server is listening on http://localhost:${PORT}`);
});
