# Instructions for running the app

1. Create a new database in your PostgreSQL Server

2. In the `register-form-app` directory run `npm install`

3. Create a `.env` file in the `/server` directory

- it should have the structure of the `.env.template` file, so you can copy it

4. Fill the values

- USER= your postgres user
- PASSWORD= your postgres password
- DATABASE= the name of your postgres database you created in (1)
- HOST= the host on which your postgres server is running (usually is localhost)
- PORT= your postgreSQL port (usually is 5432)
- NODE_PORT = your Node.js port (usually is 3000)

5. Run `npm start` to start the server

6. Open the `index.html` file to access the client
